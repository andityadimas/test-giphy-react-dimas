import React from 'react';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import IronManGiphy from './components/IronManGiphy';
import SearchGiphy from './components/SearchGiphy';

function App() {
  return (
    <Router>
      <div className="App">
        <header>
          <h1>Welcome to your Giphy App</h1>
        </header>
        <nav>
          <ul>
          <ol><Link to="/ironman">Iron Man Giphy</Link></ol>
          <ol><Link to="/search">Search Your Giphy</Link></ol>
          </ul>
        </nav>
        <main>
          <Routes>
            <Route path="/ironman" element={<IronManGiphy />} />
            <Route path="/search" element={<SearchGiphy />} />
          </Routes>
        </main>
      </div>
    </Router>
  );
}

export default App;
