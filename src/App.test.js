import React, { useState } from 'react';
import { IronManGiphy, SearchGiphy } from './components';

function App() {
  const [page, setPage] = useState('ironman');

  return (
    <div className="App">
      <header>
        <h1>Giphy App</h1>
      </header>
      <nav>
        <button onClick={() => setPage('ironman')}>Iron Man Giphy</button>
        <button onClick={() => setPage('search')}>Search Your Giphy</button>
      </nav>
      <main>
        {page === 'ironman' && <IronManGiphy />}
        {page === 'search' && <SearchGiphy />}
      </main>
    </div>
  );
}

export default App;
