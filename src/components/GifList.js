import React from 'react';
import GifItem from './GifItem';

function GifList({ gifs }) {
  return (
    <div className="gif-list">
      {gifs.map((gif) => (
        <GifItem key={gif.id} gif={gif} />
      ))}
    </div>
  );
}

export default GifList;
