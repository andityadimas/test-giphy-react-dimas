import React, { useEffect, useState } from 'react';
import './IronManGiphy.css';

function IronManGiphy() {
  const [ironManGifs, setIronManGifs] = useState([]);

  useEffect(() => {
    // Fetch Iron Man Gifs using the Giphy API
    const apiKey = '83tDmlnyvK3eaqMl2xcF0GxntvqhDBGF'; 
    const limit = 9;
    const apiUrl = `https://api.giphy.com/v1/gifs/search?q=ironman&limit=${limit}&api_key=${apiKey}`;

    fetch(apiUrl)
      .then((response) => response.json())
      .then((data) => {
        // Extract GIFs from the data response
        const gifs = data.data.map((gif) => ({
          id: gif.id,
          title: gif.title,
          url: gif.images.fixed_height.url, // Or other sizes like fixed_width, original, etc.
        }));
        setIronManGifs(gifs);
      })
      .catch((error) => {
        console.error('Error fetching Iron Man GIFs:', error);
      });

  }, []);

  return (
    <div className="ironman-giphy">
      <h2>Iron Man Giphy</h2>
      <div className="gif-list">
      {ironManGifs.map((gif) => (
          <div key={gif.id} className="gif-item">
            <img src={gif.url} alt={gif.title} />
          </div>
        ))}
      </div>
    </div>
  );
}

export default IronManGiphy;
