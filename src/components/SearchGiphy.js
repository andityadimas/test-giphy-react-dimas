import React, { useState } from 'react';
import './IronManGiphy.css';


function SearchGiphy() {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const apiKey = '83tDmlnyvK3eaqMl2xcF0GxntvqhDBGF'; // Replace with your Giphy API key

  const handleSearch = async () => {
    try {
      const response = await fetch(`https://api.giphy.com/v1/gifs/search?q=${searchQuery}&api_key=${apiKey}`);
      const data = await response.json();
      setSearchResults(data.data);
    } catch (error) {
      console.error('Error fetching Giphy data:', error);
    }
  };

  return (
    <div className="search-giphy">
      <h2>Search Your Giphy</h2>
      <input
        type="text"
        placeholder="Enter search query"
        value={searchQuery}
        onChange={(e) => setSearchQuery(e.target.value)}
      />
      <button onClick={handleSearch}>Search</button>
      <div className="gif-list">
        {searchResults.map((gif) => (
          <div key={gif.id} className="gif-item">
            <img src={gif.images.fixed_height.url} alt={gif.title} />
          </div>
        ))}
      </div>
    </div>
  );
}

export default SearchGiphy;
